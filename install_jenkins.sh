#!/bin/bash

sudo yum update –y
sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade
sudo yum install -y jenkins java-1.8.0-openjdk-devel amazon-linux-extras
sudo amazon-linux-extras enable python3.8
sudo yum install python38
#sudo yum install -y jenkins java-openjdk11
sudo systemctl daemon-reload
sudo systemctl start jenkins
sudo systemctl status jenkins