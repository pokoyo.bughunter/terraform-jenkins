variable "access_key" {
  default = "AKIAV54OSKJ6IRIMT5O3"
  description = "The AWS access key."
}

variable "secret_key" {
  default = "1iwgVxfB5rfaLNsHGQXYFzLTcjIsgOOA5saQtIvz"
  description = "The AWS secret key."
}

variable "aws_region" {
  description = "AWS region to create resources"
  default     = "us-east-1"
}

variable "vpc_id" {
  default = "Jenkins_VPC"
  description = "VPC for Jenkins"
}

variable "cidr_block" {
  default = "10.0.0.0/24"
  description = "CIDR Block to allow Jenkins Access"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "project" {
  default = "jenkins Server"
}

variable "key_name" {
  default = "Jenkins-KP"
  description = "SSH key name in your AWS account for AWS instances."
}

variable "jenkins_name" {
  description = "The name of the Jenkins server."
  default = "Jenkins_Server"
}

variable "s3_bucket" {
  default = "mycompany-jenkins"
  description = "S3 bucket where remote state and Jenkins data will be stored."
}

variable "aws_sg_name" {
  default = "Jenkins_SG"
  description = "Security Group Names"
}